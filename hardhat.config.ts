import "@typechain/hardhat";
import "@nomiclabs/hardhat-ethers";
import "@nomiclabs/hardhat-waffle";
import "solidity-coverage";
import "hardhat-gas-reporter";
import { HardhatUserConfig } from "hardhat/config";


const config: HardhatUserConfig = {
  defaultNetwork: "hardhat",
  solidity: {
    compilers: [{ version: "0.7.3", settings: {} }],
  },
  networks: {
    hardhat: {},
    localhost: {},
    coverage: {
      url: "http://127.0.0.1:8555", // Coverage launches its own ganache-cli client
    },
    rinkeby: {
      url: "https://eth-rinkeby.alchemyapi.io/v2/a4MaxLqynJ-ZuSk86OC1DBy8qEbGmRe3",
      accounts: [`0x${'ee2cba66db7655a6b344d7a1ee680075029a20c712b067d5a106ed2192e90acb'}`],
      //Esta parte no me saleeeeee
      gasPrice: 8000000000
      }
  }
};

export default config
